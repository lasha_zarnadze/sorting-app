package org.example;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.util.Arrays;
;

public class SortingApp {
    private static final Logger logger = LogManager.getLogger(SortingApp.class);

    public static void main(String[] args) {
        logger.info("SortingApp started.");

        if (args.length == 0) {
            logger.warn("No arguments provided.");
            System.out.println("No arguments provided.");
            return;
        }

        int[] numbers = new int[args.length];
        for (int i = 0; i < args.length; i++) {
            try {
                numbers[i] = Integer.parseInt(args[i]);
            } catch (NumberFormatException e) {
                logger.error("Invalid input: " + args[i]);
                System.out.println("Invalid input: " + args[i]);
                continue; // Continue processing other arguments after encountering invalid input
            }
        }

        Arrays.sort(numbers);

        logger.info("Sorted numbers: " + Arrays.toString(numbers));
        System.out.println("Sorted numbers:");
        for (int num : numbers) {
            System.out.print(num + " ");
        }
        System.out.println();
    }
}
