package org.example;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

public class TestUtils {

    private static final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private static final ByteArrayOutputStream errContent = new ByteArrayOutputStream();
    private static final PrintStream originalOut = System.out;
    private static final PrintStream originalErr = System.err;

    public static void redirectSystemOut() {
        System.setOut(new PrintStream(outContent));
        System.setErr(new PrintStream(errContent));
    }

    public static void restoreSystemOut() {
        System.setOut(originalOut);
        System.setErr(originalErr);
    }

    public static String getSystemOut() {
        return outContent.toString().trim();
    }

    public static String getSystemErr() {
        return errContent.toString().trim();
    }
}
